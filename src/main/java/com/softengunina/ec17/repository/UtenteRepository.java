package com.softengunina.ec17.repository;

import com.softengunina.ec17.entity.UtenteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UtenteRepository extends JpaRepository<UtenteEntity, UUID> {

    UtenteEntity findByEmail(@Param("email") String email);

}