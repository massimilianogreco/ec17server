package com.softengunina.ec17.repository;

import com.softengunina.ec17.entity.AcquistoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AcquistoRepository extends JpaRepository<AcquistoEntity, UUID> {

    @Query(value = "SELECT a " +
            "FROM AcquistoEntity a " +
            "WHERE a.utente.id = :idUtente")
    List<AcquistoEntity> getAcquistiByIdUtente(@Param("idUtente") UUID idUtente);

}