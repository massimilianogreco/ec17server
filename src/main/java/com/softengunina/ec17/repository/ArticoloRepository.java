package com.softengunina.ec17.repository;

import com.softengunina.ec17.entity.ArticoloEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ArticoloRepository extends JpaRepository<ArticoloEntity, UUID> {

    @Query(value = "SELECT a " +
            "FROM ArticoloEntity a " +
            "WHERE (a.quantita >= :quantitaMin) AND" +
                    "(a.prezzo >= :prezzoMin) AND" +
                    "(a.prezzo <= :prezzoMax) AND" +
                    "(:categoria = 'ALL' OR a.categoria = UPPER(:categoria) )")
    List<ArticoloEntity> getArticoliFiltered(@Param("quantitaMin") Integer quantitaMin,
                                             @Param("prezzoMin") Double prezzoMin,
                                             @Param("prezzoMax") Double prezzoMax,
                                             @Param("categoria") String categoria,
                                             Sort sort);

}