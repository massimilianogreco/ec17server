package com.softengunina.ec17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Ec17Application {

	public static void main(String[] args) {
		SpringApplication.run(Ec17Application.class, args);
	}
}
