package com.softengunina.ec17.entity.jsonMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.softengunina.ec17.entity.ArticoloEntity;
import com.softengunina.ec17.repository.ArticoloRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.UUID;

public class ArticoloMapDeserializer extends KeyDeserializer {

    @Autowired
    ArticoloRepository articoloRepository;

    @Override
    public ArticoloEntity deserializeKey(
            String key,
            DeserializationContext ctxt) throws IOException,
            JsonProcessingException {

        return articoloRepository.findOne(UUID.fromString(key));
    }
}
