package com.softengunina.ec17.entity.jsonMapper;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.softengunina.ec17.entity.ArticoloEntity;

import java.io.IOException;
import java.io.StringWriter;

public class ArticoloMapSerializer extends JsonSerializer<ArticoloEntity> {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void serialize(ArticoloEntity value,
                          JsonGenerator gen,
                          SerializerProvider serializers)
            throws IOException, JsonProcessingException {

        StringWriter writer = new StringWriter();
        mapper.writeValue(writer, value);
        gen.writeFieldName(writer.toString());
    }
}
