package com.softengunina.ec17.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.softengunina.ec17.entity.jsonMapper.ArticoloMapDeserializer;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name = "acquisto")
public class AcquistoEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @ElementCollection
    @CollectionTable(name = "articoli_acquisto", joinColumns = @JoinColumn(name = "acquisto_id"))
    @MapKeyJoinColumn(name = "articolo_id")
    @Column(name = "quantita")
    //@JsonSerialize(keyUsing = ArticoloMapSerializer.class)
    @JsonDeserialize(keyUsing = ArticoloMapDeserializer.class)
    private Map<ArticoloEntity, Integer> articoli;

    @ManyToOne
    private UtenteEntity utente;

    @Column(name = "data")
    private String data;

    @Column(name = "prezzoTotale")
    private double prezzoTotale;

    public AcquistoEntity() {
    }

    public AcquistoEntity(Map<ArticoloEntity, Integer> articoli, UtenteEntity utente, String data, double prezzoTotale) {
        this.articoli = articoli;
        this.utente = utente;
        this.data = data;
        this.prezzoTotale = prezzoTotale;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Map<ArticoloEntity, Integer> getArticoli() {
        return articoli;
    }

    public void setArticoli(Map<ArticoloEntity, Integer> articoli) {
        this.articoli = articoli;
    }

    public UtenteEntity getUtente() {
        return utente;
    }

    public void setUtente(UtenteEntity utente) {
        this.utente = utente;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getPrezzoTotale() {
        return prezzoTotale;
    }

    public void setPrezzoTotale(double prezzoTotale) {
        this.prezzoTotale = prezzoTotale;
    }

    @Override
    public String toString() {
        return "AcquistoEntity{" +
                "id=" + id +
                ", articoli=" + articoli +
                ", utente=" + utente +
                ", data='" + data + '\'' +
                ", prezzoTotale=" + prezzoTotale +
                '}';
    }
}