package com.softengunina.ec17.entity.enumerated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.softengunina.ec17.exception.BadParametersException;

public enum Categoria {
    INFORMATICA("Informatica"),
    VIDEOGIOCHI("Videogiochi"),
    ABBIGLIAMENTO("Abbigliamento"),
    LIBRI("Libri");

    private String type;

    private Categoria(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getValue(){
        return this.type.toUpperCase();
    }

    @JsonCreator
    public static Categoria create(String value) {
        try {
            if (value == null) {
                throw new BadParametersException("Bad parameters exception. Enum class doesn't accept this value: " + value);
            }
            for (Categoria v : values()) {
                if (value.equalsIgnoreCase(v.getValue())) {
                    return v;
                }
            }
            throw new BadParametersException("Bad parameters exception. Enum class doesn't accept this value: " + value);
        }catch(Exception e){}
        return null;
    }

    @JsonValue
    public String toValue() {
        for (Categoria v : values()) {
            if (this.getValue().equalsIgnoreCase(v.getValue()))
                return v.getValue();
        }
        return null;
    }
}