package com.softengunina.ec17.controller;

import com.braintreegateway.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping(value = "/api/paypal")
public class PaypalController {

    private static BraintreeGateway gateway = new BraintreeGateway(
            Environment.SANDBOX,
            "5bqtrbst7gs8zdk9",
            "xhw2w5xqdxfqkbpj",
            "c7b89ef85e6e2f91cc507152dc694b3b"
    );

    @GetMapping("/token")
    public ResponseEntity<String> getClientToken() {
        return new ResponseEntity<>(gateway.clientToken().generate(), HttpStatus.OK);
    }

    @PostMapping("/checkout")
    public ResponseEntity postPaymentMethodNonce(@RequestParam("nonce") String nonce,
                                                 @RequestParam("amount") Double amount) {

        TransactionRequest request = new TransactionRequest()
                .amount(BigDecimal.valueOf(amount))
                .paymentMethodNonce(nonce)
                .options()
                    .submitForSettlement(true)
                    .paypal()
                        .customField("PayPal custom field")
                        .description("Description for PayPal email receipt")
                        .done()
                    .done();

        Result<Transaction> result = gateway.transaction().sale(request);

        if(!result.isSuccess())
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity(HttpStatus.OK);
    }
}
