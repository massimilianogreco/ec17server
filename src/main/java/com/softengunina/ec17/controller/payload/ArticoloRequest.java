package com.softengunina.ec17.controller.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softengunina.ec17.entity.enumerated.Categoria;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

public class ArticoloRequest {

    private UUID id;

    @JsonProperty("nome")
    private String nome;

    @Enumerated(EnumType.STRING)
    @JsonProperty("categoria")
    private Categoria categoria;

    @JsonProperty("prezzo")
    private double prezzo;

    @JsonProperty("descrizione")
    private String descrizione;

    @Column(name = "quantita")
    private int quantita;

    @JsonProperty("foto")
    private Image foto;

    public ArticoloRequest() {
    }

    public ArticoloRequest(UUID id, String nome, Categoria categoria, double prezzo, String descrizione, int quantita, Image foto) {
        this.id = id;
        this.nome = nome;
        this.categoria = categoria;
        this.prezzo = prezzo;
        this.descrizione = descrizione;
        this.quantita = quantita;
        this.foto = foto;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public int getQuantita() {
        return quantita;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }

    public Image getFoto() {
        return foto;
    }

    public void setFoto(Image foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "ArticoloRequest{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", categoria=" + categoria +
                ", prezzo=" + prezzo +
                ", descrizione='" + descrizione + '\'' +
                ", quantita=" + quantita +
                ", foto=" + foto +
                '}';
    }
}
