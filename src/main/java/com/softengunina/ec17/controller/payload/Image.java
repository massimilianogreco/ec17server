package com.softengunina.ec17.controller.payload;

import java.io.Serializable;
import java.util.Arrays;

public class Image implements Serializable {

    private String name;
    private String ext;
    private int size;
    private byte[] image;

    public Image() {
    }

    public Image(String name, String ext, int size, byte[] image) {
        this.name = name;
        this.ext = ext;
        this.size = size;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Image{" +
                "name='" + name + '\'' +
                ", ext='" + ext + '\'' +
                ", size=" + size +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}