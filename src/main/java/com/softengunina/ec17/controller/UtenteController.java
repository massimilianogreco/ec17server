package com.softengunina.ec17.controller;

import com.softengunina.ec17.entity.AcquistoEntity;
import com.softengunina.ec17.entity.UtenteEntity;
import com.softengunina.ec17.repository.AcquistoRepository;
import com.softengunina.ec17.repository.UtenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/utenti")
public class UtenteController {

    @Autowired
    UtenteRepository utenteRepository;

    @Autowired
    AcquistoRepository acquistoRepository;

    @GetMapping("")
    public ResponseEntity<?> getUtenti() {
        List<UtenteEntity> utenteEntities = utenteRepository.findAll();
        if(utenteEntities.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(utenteEntities, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUtenteById(@PathVariable("id") UUID id){
        UtenteEntity utenteEntity = utenteRepository.findOne(id);
        if (utenteEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(utenteEntity, HttpStatus.OK);
    }

    @GetMapping("/email")
    public ResponseEntity<?> getUtenteByEmail(@RequestParam("email") String email){
        UtenteEntity utenteEntity = utenteRepository.findByEmail(email);
        if (utenteEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(utenteEntity, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> addUtente(@RequestBody UtenteEntity utente) {
        utenteRepository.save(utente);
        return new ResponseEntity<>(Collections.singletonMap("id", utente.getId()), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUtente(@RequestBody UtenteEntity utente, @PathVariable("id") UUID id) {
        UtenteEntity utenteOld = utenteRepository.findOne(id);
        if (utenteOld == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        utente.setId(id);
        if(utente.getPassword() == null || utente.getPassword().equals(""))
            utente.setPassword(utenteOld.getPassword());
        utenteRepository.save(utente);
        return new ResponseEntity<>(utenteRepository.findOne(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUtenteById(@PathVariable("id") UUID id){
        UtenteEntity utenteEntity = utenteRepository.findOne(id);
        if (utenteEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        for(AcquistoEntity acquisto : acquistoRepository.getAcquistiByIdUtente(id))
            acquistoRepository.delete(acquisto.getId());
        utenteRepository.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
