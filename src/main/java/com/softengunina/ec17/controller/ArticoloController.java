package com.softengunina.ec17.controller;

import com.softengunina.ec17.controller.payload.ArticoloRequest;
import com.softengunina.ec17.controller.payload.Image;
import com.softengunina.ec17.entity.ArticoloEntity;
import com.softengunina.ec17.repository.ArticoloRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/articoli")
public class ArticoloController {

    @Autowired
    ArticoloRepository articoloRepository;

    @GetMapping("")
    public ResponseEntity<?> getArticoli() {
        List<ArticoloEntity> articoloEntities = articoloRepository.findAll();
        if(articoloEntities.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(articoloEntities, HttpStatus.OK);
    }

    @GetMapping("/filter")
    public ResponseEntity<?> getArticoliFiltered(@RequestParam(value = "quantitaMin", required = false, defaultValue = "-1") Integer quantitaMin,
                                                 @RequestParam(value = "prezzoMin", required = false, defaultValue = "0.0") Double prezzoMin,
                                                 @RequestParam(value = "prezzoMax", required = false, defaultValue = "1000000.0") Double prezzoMax,
                                                 @RequestParam(value = "categoria", required = false, defaultValue = "ALL") String categoria,
                                                 @RequestParam(value = "orderBy", required = false) String orderBy,
                                                 @RequestParam(value = "directionSort", required = false, defaultValue = "0") Integer directionSort) {
        List<ArticoloEntity> articoloEntities;
        if(orderBy == null)
            articoloEntities = articoloRepository.getArticoliFiltered(quantitaMin,prezzoMin,prezzoMax,categoria,null);
        else if(directionSort == 1)
            articoloEntities = articoloRepository.getArticoliFiltered(quantitaMin,prezzoMin,prezzoMax,categoria,new Sort(Sort.Direction.DESC, orderBy));
        else
            articoloEntities = articoloRepository.getArticoliFiltered(quantitaMin,prezzoMin,prezzoMax,categoria,new Sort(Sort.Direction.ASC, orderBy));
        if(articoloEntities.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(articoloEntities, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getArticoloById(@PathVariable("id") UUID id){
        ArticoloEntity articoloEntity = articoloRepository.findOne(id);
        if (articoloEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(articoloEntity, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> addArticolo(@RequestBody ArticoloRequest articolo) {
        ArticoloEntity newArticolo = new ArticoloEntity(
                articolo.getNome(),
                articolo.getCategoria(),
                articolo.getPrezzo(),
                articolo.getDescrizione(),
                articolo.getQuantita(),
                null
        );
        if(articolo.getFoto()!=null) {
            articoloRepository.save(newArticolo);
            newArticolo.setFoto(fromImageToUrl(articolo.getFoto(), newArticolo.getId()));
        }
        articoloRepository.save(newArticolo); //salva (o aggiorna) articolo
        return new ResponseEntity<>(Collections.singletonMap("id", newArticolo.getId()), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateArticolo(@RequestBody ArticoloRequest articolo, @PathVariable("id") UUID id) {
        ArticoloEntity articoloEntity = articoloRepository.findOne(id);
        if (articoloEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        articolo.setId(id);

        articoloEntity.setNome(articolo.getNome());
        articoloEntity.setCategoria(articolo.getCategoria());
        articoloEntity.setPrezzo(articolo.getPrezzo());
        articoloEntity.setDescrizione(articolo.getDescrizione());
        articoloEntity.setQuantita(articolo.getQuantita());
        if(articolo.getFoto()!=null && articoloEntity.getFoto()!=null) {
            File immagine = new File("/var/www/html/" + articoloEntity.getFoto().substring(54));
            immagine.delete();
            articoloEntity.setFoto(fromImageToUrl(articolo.getFoto(), id));
        }

        articoloRepository.save(articoloEntity);
        return new ResponseEntity<>(articoloRepository.findOne(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteArticoloById(@PathVariable("id") UUID id){
        ArticoloEntity articoloEntity = articoloRepository.findOne(id);
        if (articoloEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(articoloEntity.getFoto()!=null) {
            File immagine = new File("/var/www/html/" + articoloEntity.getFoto().substring(54));
            immagine.delete();
        }
        articoloRepository.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/riduciquantita")
    public ResponseEntity<?> riduciQuantita(@RequestBody List<ArticoloEntity> articoli){
        ArticoloEntity articoloDb;
        for(ArticoloEntity articolo : articoli) {
            articoloDb = articoloRepository.findOne(articolo.getId());
            if (articoloDb == null || articoloDb.getQuantita() < articolo.getQuantita())
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        for(ArticoloEntity articolo : articoli){
            articoloDb = articoloRepository.findOne(articolo.getId());
            articoloDb.setQuantita(articoloDb.getQuantita()-articolo.getQuantita());
            articoloRepository.save(articoloDb);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/rifornisciquantita")
    public ResponseEntity<?> rifornisciQuantita(@RequestBody List<ArticoloEntity> articoli){
        ArticoloEntity articoloDb;
        for(ArticoloEntity articolo : articoli) {
            articoloDb = articoloRepository.findOne(articolo.getId());
            if (articoloDb != null){
                articoloDb.setQuantita(articoloDb.getQuantita()+articolo.getQuantita());
                articoloRepository.save(articoloDb);
            }
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private String fromImageToUrl(Image image, UUID uuid){
        String nameFile = uuid + "." + image.getExt();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("/var/www/html/" + nameFile);
            fileOutputStream.write(image.getImage(), 0, image.getSize());
            fileOutputStream.close();
        } catch (IOException e) {
            return null;
        }
        return "http://ec2-3-8-20-158.eu-west-2.compute.amazonaws.com/" + nameFile;
    }

}
