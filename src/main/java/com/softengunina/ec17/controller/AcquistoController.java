package com.softengunina.ec17.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.softengunina.ec17.entity.AcquistoEntity;
import com.softengunina.ec17.entity.ArticoloEntity;
import com.softengunina.ec17.entity.UtenteEntity;
import com.softengunina.ec17.repository.AcquistoRepository;
import com.softengunina.ec17.repository.ArticoloRepository;
import com.softengunina.ec17.repository.UtenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

@RestController
@RequestMapping(value = "/api/acquisti")
public class AcquistoController {

    @Autowired
    AcquistoRepository acquistoRepository;

    @Autowired
    ArticoloRepository articoloRepository;

    @Autowired
    UtenteRepository utenteRepository;

    private static final String fatturePath = System.getProperty("user.home") + "/Fatture/";

    @GetMapping("")
    public ResponseEntity<?> getAcquisti() {
        List<AcquistoEntity> acquistoEntities = acquistoRepository.findAll();
        if(acquistoEntities.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(acquistoEntities, HttpStatus.OK);
    }

    @GetMapping("/utente/{id}")
    public ResponseEntity<?> getAcquistiUtente(@PathVariable("id") UUID idUtente) {
        List<AcquistoEntity> acquistoEntities = acquistoRepository.getAcquistiByIdUtente(idUtente);
        if(acquistoEntities.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(acquistoEntities, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAcquistoById(@PathVariable("id") UUID id){
        AcquistoEntity acquistoEntity = acquistoRepository.findOne(id);
        if (acquistoEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(acquistoEntity, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> addAcquisto(@RequestBody AcquistoEntity acquisto) {

        acquisto.setUtente(utenteRepository.findOne(acquisto.getUtente().getId()));

        SimpleDateFormat localDate = new SimpleDateFormat();
        localDate.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        localDate.applyPattern("dd/MM/yyyy HH.mm");
        acquisto.setData(localDate.format(new Date()));

        double totAcquisto = 0;
        for(ArticoloEntity articolo : acquisto.getArticoli().keySet())
            totAcquisto += articolo.getPrezzo()*acquisto.getArticoli().get(articolo);
        acquisto.setPrezzoTotale(Math.round(totAcquisto * 100.0) / 100.0);

        acquistoRepository.save(acquisto);

        inviaFattura(acquisto);

        return new ResponseEntity<>(Collections.singletonMap("id", acquisto.getId()), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAcquistoById(@PathVariable("id") UUID id){
        AcquistoEntity acquistoEntity = acquistoRepository.findOne(id);
        if (acquistoEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        acquistoRepository.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/fattura/{id}")
    public ResponseEntity<?> getFatturaAcquisto(@PathVariable("id") UUID id){
        AcquistoEntity acquistoEntity = acquistoRepository.findOne(id);
        if (acquistoEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        byte[] fattura;
        String fatturaEncoded;
        try {
            fattura = Files.readAllBytes(Paths.get(fatturePath + id.toString() + ".pdf"));
            fatturaEncoded = Base64.getEncoder().encodeToString(fattura);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(Collections.singletonMap("fattura", fatturaEncoded), HttpStatus.OK);
    }

    private void inviaFattura(AcquistoEntity acquisto)
    {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");

        final String username = "ec17mail@gmail.com";
        final String password = "ec17ec17";

        Session session = Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message msg = new MimeMessage(session);

        try {
            InternetAddress addressFrom = new InternetAddress("fattura@ec17.com");
            msg.setFrom(addressFrom);
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(acquisto.getUtente().getEmail()));
            msg.setSubject("Fattura acquisto EC-17");

            Multipart multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText("Gentile cliente,\n" +
                    "in allegato la fattura relativa al suo acquisto.");
            multipart.addBodyPart(messageBodyPart);

            generaFattura(acquisto);
            messageBodyPart = new MimeBodyPart();
            String filename = fatturePath + acquisto.getId() + ".pdf";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName("Fattura EC-17.pdf");
            multipart.addBodyPart(messageBodyPart);

            msg.setContent(multipart);

            Transport.send(msg);

        }catch (MessagingException e){}
    }

    private void generaFattura(AcquistoEntity acquisto){
        try {
            OutputStream file = new FileOutputStream(new File(fatturePath + acquisto.getId() + ".pdf"));

            Document document = new Document();
            PdfWriter.getInstance(document, file);
            document.open();

            Font smallBold  = new Font(Font.FontFamily.TIMES_ROMAN, 12,  Font.BOLD);
            Font bigFont  = new Font(Font.FontFamily.TIMES_ROMAN, 18,  Font.BOLD);

            document.add(new Paragraph("EC-17", new Font(Font.FontFamily.TIMES_ROMAN, 20,  Font.BOLD)));
            document.add(new Paragraph(" "));

            Paragraph paragraph = new Paragraph("Fattura", bigFont);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);
            document.add(new Paragraph(" "));

            UtenteEntity utente = acquisto.getUtente();
            document.add(new Paragraph("Indirizzo di spedizione:", smallBold));
            document.add(new Paragraph(utente.getNome() + " " + utente.getCognome()));
            document.add(new Paragraph(utente.getIndirizzo()));
            document.add(new Paragraph(" "));

            document.add(new Paragraph("Numero ordine:", smallBold));
            document.add(new Paragraph(String.valueOf(acquisto.getId())));
            document.add(new Paragraph(" "));

            Locale.setDefault(Locale.ITALIAN);
            document.add(new Paragraph("Data ordine:", smallBold));
            document.add(new Paragraph(acquisto.getData()));
            document.add(new Paragraph(" "));

            PdfPTable tabella = new PdfPTable(4);
            tabella.setWidthPercentage(100);
            tabella.setWidths(new float[] {50, 10, 20, 20});

            PdfPCell c1 = new PdfPCell(new Phrase("Descrizione", smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
            tabella.addCell(c1);

            c1 = new PdfPCell(new Phrase("Quantità", smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
            tabella.addCell(c1);

            c1 = new PdfPCell(new Phrase("Prezzo unitario", smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
            tabella.addCell(c1);

            c1 = new PdfPCell(new Phrase("Prezzo totale", smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
            tabella.addCell(c1);

            double tot;
            int quantita;
            for(ArticoloEntity articolo : acquisto.getArticoli().keySet()){
                tabella.addCell(articolo.getNome());

                quantita = acquisto.getArticoli().get(articolo);
                c1 = new PdfPCell(new Paragraph(String.valueOf(quantita)));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                tabella.addCell(c1);

                c1 = new PdfPCell(new Paragraph(articolo.getPrezzo() + " €"));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                tabella.addCell(c1);

                tot = Math.round(articolo.getPrezzo()*quantita * 100.0) / 100.0;
                c1 = new PdfPCell(new Paragraph(tot + " €"));
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                tabella.addCell(c1);
            }

            c1 = new PdfPCell();
            c1.setBorderWidth(0);
            tabella.addCell(c1);
            c1 = new PdfPCell();
            c1.setBorderWidth(0);
            tabella.addCell(c1);

            c1 = new PdfPCell(new Paragraph("TOTALE: ", smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            c1.setBorderWidth(0);
            tabella.addCell(c1);

            c1 = new PdfPCell(new Paragraph(acquisto.getPrezzoTotale() + " €", smallBold));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
            tabella.addCell(c1);

            document.add(tabella);

            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
