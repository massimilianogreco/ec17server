package com.softengunina.ec17.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException{

    public BadRequestException(){
        super();
    }

    public BadRequestException(String message){
        super(message);
    }

    public BadRequestException(String message, Throwable e){
        super(message, e);
    }
}
