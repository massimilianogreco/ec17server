package com.softengunina.ec17.exception.payload;

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;

import java.util.HashMap;
import java.util.Map;

@Component
public class CustomExceptionPayload extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {

        Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);

        Throwable throwable = getError(requestAttributes);
        if(throwable != null && throwable.getCause() != null) {
            Throwable cause = throwable.getCause();
            Map<String, Object> causeErrorAttributes = new HashMap<>();
            causeErrorAttributes.put("exception", cause.getClass().getName());
            causeErrorAttributes.put("message", cause.getMessage());
            errorAttributes.put("details", causeErrorAttributes);
        }
        else{
            errorAttributes.remove("exception");
        }

        Object statusValue = errorAttributes.get("status");

        if(statusValue != null){
            errorAttributes.remove("status");
            errorAttributes.put("code", statusValue);
        }
        return errorAttributes;
    }
}
