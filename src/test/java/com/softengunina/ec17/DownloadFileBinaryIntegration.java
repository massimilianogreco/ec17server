package com.softengunina.ec17;

import com.softengunina.ec17.exception.InternalException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class DownloadFileBinaryIntegration {

    static{
        // This is to initialized the centralized log (check in logback.xml)
        System.setProperty("syslog.server", "localhost");
        System.setProperty("syslog.facility", "SYSLOG");

    }

    public static void main (String args[]) throws IOException {

        saveInputStream(null, null);
    }


    private static File saveInputStream(String url, String destination) throws IOException {

        Response response = callURL(url);

        File targetFile;

        try (InputStream initialStream = response.body().byteStream()) {

            targetFile = new File(destination);

            FileUtils.copyInputStreamToFile(initialStream, targetFile);
        }

        return targetFile;
    }


    private static Response callURL(String url) throws IOException {
        if (url == null || url.isEmpty()) {
            throw new InternalException("URL Input source is not valorized");
        }

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        return client.newCall(request).execute();
    }
}
